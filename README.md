# Webarchitects Ansible Drush Role

This role:

1. Installs Drush 8.x to `/usr/local/bin/drush8` based on the version in the
   [defaults file](defaults/main.yml).
2. Installs Drush Launcher to `/usr/local/bin/drush` based on the version in the
   [defaults file](defaults/main.yml).

See the [Drush](https://github.com/drush-ops/drush) and [Drush
Launcher](https://github.com/drush-ops/drush-launcher) projects for details.
