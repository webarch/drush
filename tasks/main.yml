# Copyright 2019-2024 Chris Croome
#
# This file is part of the Webarchitects Drush Ansible role.
#
# The Webarchitects Drush Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Drush Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Drush Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Skip the tasks in this role as drush is false
  ansible.builtin.debug:
    msg: "The drush variable need to be true for the tasks in this role to run."
  when: not drush | bool
  tags:
    - drush

- name: Install Drush 8 and Drush launcher
  block:

    - name: Debug variables
      ansible.builtin.debug:
        msg:
          - "The proposed Drush 8 version is: {{ drush8_version }}"
          - "The proposed Drush Launcher version is: {{ drush_launcher_version }}"
        verbosity: 1

    - name: Packages present
      ansible.builtin.apt:
        pkg:
          - php-cli
        update-cache: true
        state: present

    - name: Legacy Drush assets absent
      ansible.builtin.file:
        path: "{{ path }}"
        state: absent
      loop: "{{ drush_legacy_paths }}"
      loop_control:
        loop_var: path
        label: "{{ path | basename }}"
      when:
        - drush_legacy_paths is defined
        - drush_legacy_paths != []

    - name: Include Drush 8 tasks
      ansible.builtin.include_tasks: drush8.yml
      tags:
        - drush8

    - name: Include Drush Launcher tasks
      ansible.builtin.include_tasks: drush_launcher.yml
      tags:
        - drush_launcher

  when: drush | bool
  tags:
    - drush
...
